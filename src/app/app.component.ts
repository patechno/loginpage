import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loginForm: FormGroup;
  usreNameErrors = ['length', 'begin', 'number'];
  passwordErrors = ['length', 'other'];

  constructor() { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(null, [Validators.required, this.lengthValidator(), this.beginWithAlaphbet(), this.contrainsNumber()]),
      password: new FormControl(null, [Validators.required, this.lengthValidator(), this.passwordValidator()])
    });
  }

  login() {
    console.log('login button was clicked');
  }

  lengthValidator(min = 8, max = 20): ValidatorFn {
    return (control: AbstractControl) => {
      const value = control.value;
      if (!value || value.length < min || value.length > max) {
        return {
          length: true
        };
      }
      return null;
    };
  }

  beginWithAlaphbet(): ValidatorFn {
    return (control: AbstractControl) => {
      if (!control.value || !(/^[a-z]+[a-z0-9]*$/i).test(control.value)) {
        return {
          begin: true
        };
      }
      return null;
    };
  }

  contrainsNumber(): ValidatorFn {
    return (control: AbstractControl) => {
      if (!control.value || !(/^.*[0-9].*$/i).test(control.value)) {
        return {
          number: true
        };
      }
      return null;
    };
  }

  passwordValidator(): ValidatorFn {
    return (control: AbstractControl) => {
      if (!control.value || !(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -/:-@\[-`{-~!]).{4,20}$/).test(control.value)) {
        return {
          password: true
        };
      }
      return null;
    };
  }

}
